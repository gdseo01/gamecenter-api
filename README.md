* OAuth2.0

1. access token 은 JWT 를 사용.
2. JWT payload 부분에 유효기간과 customerId, jti token 이 포함되어있음.
3. token expire_in 은 oauth_clients_detail 테이블 참고.
4. JWT 를 사용하기 때문에 oauth_access_token, oauth_approvals, oauth_client_token, oauth_code, oauth_refresh_token 테이블은 사용하지 않음.


* API Docs

    http://api-docs.admerce.co.kr/gamecenter/

    http://api-docs.admerce.co.kr/gamecenter-admin/


* Staging 에 배포하는 방법

1. bastion 서버로 파일 업로드

    scp -i ~/.ssh/stg-gamecenter-key-pair.pem resource.jar파일경로 ec2-user@13.125.26.70:/home/ec2-user
   
    scp -i ~/.ssh/stg-gamecenter-key-pair.pem oauth.jar파일경로 ec2-user@13.125.26.70:/home/ec2-user
   
   
2. bastion 서버에 로그인

    ssh -i ~/.ssh/stg-gamecenter-key-pair.pem ec2-user@13.125.26.70
   
   
3. external api 서버로 파일 업로드

    distribute-ext-api01 입력 후 엔터
   
    distribute-ext-api02 입력 후 엔터
   
    distribute-auth 입력 후 엔터
   
   
4. ext-api01 서버 접속

    ssh -i ~/.ssh/stg-gamecenter-key-pair.pem ec2-user@10.0.1.40
   
    sudo su
   
    kill `ps -ef | grep java | grep -v grep | awk '{print $2}'`
   
    nohup java -jar resource.jar & 
   
   
5. ext-api02 서버 접속

    ssh -i ~/.ssh/stg-gamecenter-key-pair.pem ec2-user@10.0.1.104
   
    sudo su
   
    kill `ps -ef | grep java | grep -v grep | awk '{print $2}'`
   
    nohup java -jar resource.jar &
   
   
6. oauth 서버 접속

    ssh -i ~/.ssh/stg-gamecenter-key-pair.pem ec2-user@10.0.1.142
   
    sudo su
   
    kill `ps -ef | grep java | grep -v grep | awk '{print $2}'`
   
    nohup java -jar oauth.jar &
   