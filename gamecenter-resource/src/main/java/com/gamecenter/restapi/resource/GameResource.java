package com.gamecenter.restapi.resource;

import com.gamecenter.restapi.domain.Game;
import com.gamecenter.restapi.dto.GameDTO;
import com.gamecenter.restapi.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Path("/v1/Games")
public class GameResource {

    @Autowired
    private GameService gameService;

    public GameResource() {

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<GameDTO> getAllGames() {
        List<GameDTO> gameDTOs = gameService.getAllGames();

        return gameDTOs;
    }

    @GET
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GameDTO getGameById(@PathParam("id") Long gameId) {
        OAuth2AuthenticationDetails authenticationDetails = (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        Map<String, Object> details = (HashMap<String, Object>) authenticationDetails.getDecodedDetails();
        Long customerId = new Long(details.get("customerId").toString());

        Game game = new Game();
        game.setCustomerId(customerId);
        game.setGameId(gameId);

        return gameService.getGameById(game);
    }

//    @POST
//    @Consumes(MediaType.MULTIPART_FORM_DATA)
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response addGame(@FormDataParam("title") String title
//                            , @FormDataParam("customerId") int customerId
//                            , @FormDataParam("description") String description
//                            , @FormDataParam("images[logo_img]") InputStream logoImgStream
//                            , @FormDataParam("images[logo_img]") FormDataContentDisposition logoImgMetaData
//                            , @FormDataParam("images[story_img]") InputStream storyImgStream
//                            , @FormDataParam("images[story_img]") FormDataContentDisposition storyImgMetaData
//                            , @FormDataParam("images[explanation_img]") InputStream explanationImgStream
//                            , @FormDataParam("images[explanation_img]") FormDataContentDisposition explanationImgMetaData
//                            , @FormDataParam("zip[game_zip]") InputStream gameZipStream
//                            , @FormDataParam("zip[game_zip]") FormDataContentDisposition gameZipMetaData
//                            , @Context UriInfo uriInfo) {
//
////        Customer customer = new Customer();
////        Long customerId = customerService.findCustomerId(loginId);
//
//        Game game = new Game();
//        game.setTitle(title);
//        game.setCustomerId(customerId);
//        game.setDescription(description);
//
//        Game newGameEntity = gameService.addGame(game, logoImgStream, logoImgMetaData, storyImgStream, storyImgMetaData, explanationImgStream, explanationImgMetaData, gameZipStream, gameZipMetaData);
//        URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(newGameEntity.getGameId())).build();
//
//        return Response.created(uri).entity(newGameEntity).build();
//    }
}
