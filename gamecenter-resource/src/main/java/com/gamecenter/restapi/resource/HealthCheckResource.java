package com.gamecenter.restapi.resource;

import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Component
@Path("/v1/HealthCheck")
public class HealthCheckResource {

    public HealthCheckResource() {

    }

    @GET
    public Response getHealthCheck() {
        return Response.ok().build();
    }

}
