package com.gamecenter.restapi.resource;

import com.gamecenter.restapi.domain.Contract;
import com.gamecenter.restapi.dto.ContractDTO;
import com.gamecenter.restapi.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Component
@Path("/v1/Contracts")
public class ContractResource {

    @Autowired
    private ContractService contractService;

    public ContractResource() {

    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ContractDTO getContract(@PathParam("id") Long contractId) {
        OAuth2AuthenticationDetails authenticationDetails = (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        Map<String, Object> details = (HashMap<String, Object>) authenticationDetails.getDecodedDetails();
        Long customerId = new Long(details.get("customerId").toString());

        Contract contract = new Contract();
        contract.setContractId(contractId);
        contract.setCustomerId(customerId);

        return contractService.getContractById(contract);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addContract(Contract contract, @Context UriInfo uriInfo) {
        OAuth2AuthenticationDetails authenticationDetails = (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        Map<String, Object> details = (HashMap<String, Object>) authenticationDetails.getDecodedDetails();
        Long customerId = new Long(details.get("customerId").toString());

        contract.setCustomerId(customerId);

        Long contractId = contractService.addContract(contract);
        URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(contractId)).build();

        return Response.created(uri).build();

    }
}
