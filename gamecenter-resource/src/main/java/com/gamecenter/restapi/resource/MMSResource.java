package com.gamecenter.restapi.resource;

import com.gamecenter.restapi.domain.MMSRequest;
import com.gamecenter.restapi.dto.MMSRequestDTO;
import com.gamecenter.restapi.service.MMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Component
@Path("/v1/MMSes")
public class MMSResource {

    @Autowired
    private MMSService mmsService;

    public MMSResource() {

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendMMSes(MMSRequest mms) throws IOException {
        mmsService.sendMMSes(mms);

        return Response.noContent().build();
    }
}
