package com.gamecenter.restapi.resource;

import com.gamecenter.restapi.domain.UrlHash;
import com.gamecenter.restapi.dto.UrlHashDTO;
import com.gamecenter.restapi.service.URLHashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

@Component
@Path("/v1/URLs")
public class URLHashResource {

    @Autowired
    private URLHashService urlHashService;

    public URLHashResource() {

    }

    @GET
    @Path("/{hash}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUrlByHash(@PathParam("hash") String hash) {
        UrlHash urlHash = new UrlHash();
        urlHash.setHash(hash);

        UrlHashDTO newUrlHash = urlHashService.getUrlByHash(urlHash);

        return Response.status(Response.Status.TEMPORARY_REDIRECT).header("Location", newUrlHash.getUrl()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUrlHash(UrlHash urlHash, @Context UriInfo uriInfo) {
        String hash = urlHashService.addUrlHash(urlHash);
        URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(hash)).build();

        return Response.created(uri).build();
    }
}
