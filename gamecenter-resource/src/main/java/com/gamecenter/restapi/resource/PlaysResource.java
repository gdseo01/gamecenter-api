package com.gamecenter.restapi.resource;

import com.gamecenter.restapi.constraints.GameEnd;
import com.gamecenter.restapi.constraints.GameStart;
import com.gamecenter.restapi.domain.Contract;
import com.gamecenter.restapi.domain.GamePlay;
import com.gamecenter.restapi.dto.ContractDTO;
import com.gamecenter.restapi.dto.GamePlayDTO;
import com.gamecenter.restapi.dto.RankDTO;
import com.gamecenter.restapi.service.PlaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/v1/Plays")
public class PlaysResource {

    @Autowired
    PlaysService playsService;

    public PlaysResource() {

    }

    @GET
    @Path("/metadata/{contractId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ContractDTO getContractGameOptions(@PathParam(value = "contractId") Long contractId) {
        Contract contract = new Contract();
        contract.setContractId(contractId);

        return playsService.getCustomGameOptions(contract);
    }

    @GET
    @Path("/validate/{contractId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateByContractId(@PathParam(value = "contractId") Long contractId,
                                        @QueryParam(value = "userKey") String userKey) {

        Contract contract = new Contract();
        contract.setContractId(contractId);

        GamePlay gamePlay = new GamePlay();
        gamePlay.setUserKey(userKey);

        playsService.validateGameStart(contract, gamePlay);

        return Response.noContent().build();
    }

    @GET
    @Deprecated
    @Path("/{customerId}/{contractId}/validate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateByContractId2(@PathParam(value = "customerId") Long customerId,
                                         @PathParam(value = "contractId") Long contractId,
                                         @QueryParam(value = "userKey") String userKey) {
        Contract contract = new Contract();
        contract.setContractId(contractId);
        contract.setCustomerId(customerId);

        GamePlay gamePlay = new GamePlay();
        gamePlay.setUserKey(userKey);

        playsService.validateGameStart(contract, gamePlay);

        return Response.noContent().build();
    }

    @POST
    @Path("/start")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response gameStart(@GameStart GamePlay gamePlay) {
        gamePlay.setActionType("S");
        gamePlay.setPlatformId(1L);

        playsService.gameStart(gamePlay);

        return Response.noContent().build();
    }

    @POST
    @Path("/end")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response gameEnd(@GameEnd GamePlay gamePlay) {
        gamePlay.setActionType("E");
        gamePlay.setPlatformId(1L);

        playsService.gameEnd(gamePlay);

        return Response.noContent().build();
    }

    @GET
    @Path("/rank/{contractId}")
    @Produces(MediaType.APPLICATION_JSON)
    public RankDTO getCurrentRank(@PathParam(value = "contractId") Long contractId,
                                  @QueryParam(value = "userKey") String userKey) {

        GamePlay gamePlay = new GamePlay();
        gamePlay.setContractId(contractId);
        gamePlay.setUserKey(userKey);

        RankDTO rankDTO = playsService.getCurrentRank(gamePlay);

        return rankDTO;
    }
}
