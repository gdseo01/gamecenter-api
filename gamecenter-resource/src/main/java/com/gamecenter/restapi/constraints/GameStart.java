package com.gamecenter.restapi.constraints;

import com.gamecenter.restapi.domain.GamePlay;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = GameStart.Validator.class)
public @interface GameStart {
    String message() default "constraints gamestart message.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class Validator implements ConstraintValidator<GameStart, GamePlay> {
        @Override
        public void initialize(GameStart gameStart) {

        }

        @Override
        public boolean isValid(GamePlay gamePlay, ConstraintValidatorContext constraintValidatorContext) {
            if (gamePlay == null) {
                return false;

            } else {
                boolean isValid = true;

                if (gamePlay.getGameId() == null) {
                    constraintValidatorContext.disableDefaultConstraintViolation();
                    constraintValidatorContext
                            .buildConstraintViolationWithTemplate("gameId {validation.message.notnull}")
                            .addConstraintViolation();
                }

                if (gamePlay.getContractId() == null) {
                    constraintValidatorContext.disableDefaultConstraintViolation();
                    constraintValidatorContext
                            .buildConstraintViolationWithTemplate("contractId {validation.message.notnull}")
                            .addConstraintViolation();
                    isValid = false;
                }

//                if (gamePlay.getPlatformId() == null) {
//                    constraintValidatorContext
//                            .buildConstraintViolationWithTemplate("platformId {validation.message.notnull}")
//                            .addConstraintViolation();
//                    isValid = false;
//
//                }

                if (gamePlay.getUserKey() == null) {
                    constraintValidatorContext
                            .buildConstraintViolationWithTemplate("userKey {validation.message.notnull}")
                            .addConstraintViolation();
                    isValid = false;
                }

                if (gamePlay.getUserKeyType() == null) {
                    constraintValidatorContext
                            .buildConstraintViolationWithTemplate("userKeyType {validation.message.notnull}")
                            .addConstraintViolation();
                    isValid = false;
                }

                // null 확인을 먼저 확인하고 isValid 가 flase 이면 return 한다.
                if (isValid == false) {
                    return false;
                }

                if (!(gamePlay.getGameId() instanceof Number)) {
                    constraintValidatorContext
                            .buildConstraintViolationWithTemplate("gameId {validation.message.invalidtype}")
                            .addConstraintViolation();
                    isValid = false;
                }

                if (isValid == false) {
                    return false;
                }

                if (gamePlay.getGameId() <= 0) {
                    constraintValidatorContext.disableDefaultConstraintViolation();
                    constraintValidatorContext
                            .buildConstraintViolationWithTemplate("gameId {validation.message.gt} 0.")
                            .addConstraintViolation();
                    isValid = false;
                }

                if (gamePlay.getContractId() <= 0) {
                    constraintValidatorContext.disableDefaultConstraintViolation();
                    constraintValidatorContext
                            .buildConstraintViolationWithTemplate("contrantId {validation.message.gt} 0.")
                            .addConstraintViolation();
                    isValid = false;
                }

                return isValid;
            }
        }
    }
}
