package com.gamecenter.restapi.constraints;

import com.gamecenter.restapi.domain.GamePlay;
import com.gamecenter.restapi.dto.GamePlayDTO;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = GameEnd.Validator.class)
public @interface GameEnd {
    String message() default "{com.gamecenter.restapi.constraints.GameEnd.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class Validator implements ConstraintValidator<GameEnd, GamePlay> {
        @Override
        public void initialize(GameEnd gameEnd) {

        }

        @Override
        public boolean isValid(GamePlay gamePlay, ConstraintValidatorContext constraintValidatorContext) {
            if (gamePlay == null) {
                return false;

            } else {
                return (gamePlay.getGameId() > 0) && (gamePlay.getContractId() != null)
                        && (gamePlay.getUserKey() != null)
                        && (gamePlay.getUserKeyType() != null) && (gamePlay.getScore() > -1);
            }
        }
    }
}
