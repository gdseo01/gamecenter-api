package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.Contract;
import com.gamecenter.restapi.domain.GamePlay;
import com.gamecenter.restapi.dto.ContractDTO;
import com.gamecenter.restapi.dto.RankDTO;

public interface PlaysService {
    ContractDTO getCustomGameOptions(Contract contract);

    Boolean validateGameStart(Contract contract, GamePlay gamePlay);

    void gameStart(GamePlay gamePlay);

    void gameEnd(GamePlay gamePlay);

    RankDTO getCurrentRank(GamePlay gamePlay);
}
