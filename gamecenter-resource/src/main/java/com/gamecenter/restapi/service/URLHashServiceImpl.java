package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.UrlHash;
import com.gamecenter.restapi.dto.UrlHashDTO;
import com.gamecenter.restapi.mappers.URLHashMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import java.util.UUID;

@Service
public class URLHashServiceImpl implements URLHashService {

    @Autowired
    private URLHashMapper urlHashMapper;

    @Override
    @Transactional(readOnly = false)
    public String addUrlHash(UrlHash urlHash) {
        try {
            UrlHashDTO newUrlHash = getUrlByHash(urlHash);

            if (newUrlHash == null) {
                UUID uuid = UUID.randomUUID();

                urlHash.setHash(uuid.toString());
                urlHashMapper.insertUrlHash(urlHash);

                return urlHash.getHash();

            } else {
                return newUrlHash.getHash();
            }

        } catch (Exception exception) {
            throw new InternalServerErrorException();
        }

    }

    @Override
    @Transactional(readOnly = true)
    public UrlHashDTO getUrlByHash(UrlHash urlHash) {
        try {
            UrlHash urlHash1 = urlHashMapper.selectUrlHashByHash(urlHash);

            if (urlHash1 == null) {
                throw new NotFoundException("URL not found.");
            }

            ModelMapper modelMapper = new ModelMapper();
            UrlHashDTO newUrlHash = modelMapper.map(urlHash1, UrlHashDTO.class);

            return newUrlHash;

        } catch (Exception exception) {
            throw new InternalServerErrorException();
        }

    }
}
