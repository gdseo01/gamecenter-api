package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.MMSRequest;
import com.gamecenter.restapi.domain.MMSResponse;
import com.gamecenter.restapi.dto.MMSRequestDTO;
import com.gamecenter.restapi.mappers.MMSRequestMapper;
import com.gamecenter.restapi.mappers.MMSResponseMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.InternalServerErrorException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Base64;

@Service
public class MMSServiceImpl implements MMSService {

    @Autowired
    private MMSRequestMapper mmsRequestMapper;

    @Autowired
    private MMSResponseMapper mmsResponseMapper;

    @Value(value="${aoa.id}")
    private String aoaId;

    @Value(value="${aoa.password}")
    private String aoaPassword;

    @Value(value="${aoa.recv-phone}")
    private String aoaRecvPhone;

    @Override
    public MMSRequestDTO sendMMSes(MMSRequest mms) throws IOException {

        mmsRequestMapper.insertMMSRequest(mms);

        MultiValueMap<String, Object> valueMap = new LinkedMultiValueMap<>();
        valueMap.add("id", aoaId);
        valueMap.add("pwd", aoaPassword);
        valueMap.add("gubun", mms.getGubun());
        valueMap.add("send_phone", mms.getSendPhone());

        if (aoaRecvPhone == null) {
            valueMap.add("recv_phone", mms.getRecvPhone());
        } else {
            valueMap.add("revc_phone", aoaRecvPhone);
        }

        valueMap.add("subject", iconvWithBase64Encoding("UTF-8", "EUC-KR", mms.getSubject()));
        valueMap.add("txt", iconvWithBase64Encoding("UTF-8", "EUC-KR", mms.getMsg()));
        valueMap.add("returntype", "TEXT");
        valueMap.add("send_key", "gc" + mms.getMmsRequestId());

        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().clear();
//        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("EUC-KR")));

//        HttpHeaders httpHeaders = new HttpHeaders();
//        httpHeaders.add("Content-Type", "application/x-www-form-urlencoded");
//        httpHeaders.add("Accept-Language", "ko_KR");
//        httpHeaders.add("Accept-Charset", "euc-kr");

//        HttpEntity httpEntity = new HttpEntity<>(valueMap, httpHeaders);
//        ResponseEntity<String> responseText = restTemplate.exchange("http://aoa.sktelecom.com/newmms/ext_message.html", HttpMethod.POST, httpEntity, String.class);
//        String[] arrayText = responseText.toString().split(",");

        String responseText = restTemplate.postForObject("http://aoa.sktelecom.com/newmms/ext_message.html", valueMap, String.class);
        String[] arrayText = responseText.split(",");

        MMSResponse mmsResponse = new MMSResponse();

        try {
            String key;
            String value;

            for (String keyValuePair : arrayText) {
                key = "";
                value = "";

                String[] tmp = keyValuePair.split("=");

                key = tmp[0];

                if (tmp.length > 1) {
                    value = tmp[1];
                }

                switch (key) {
                    case "send_key":
                        mmsResponse.setSendKey(value);
                        mmsResponse.setMmsRequestId(Long.parseLong(value.replace("gc", "")));
                        break;

                    case "unique":
                        mmsResponse.setMmsResponseId(Long.parseLong(value));
                        break;

                    case "msgkey":
                        mmsResponse.setMsgKey(value);
                        break;

                    case "code":
                        mmsResponse.setCode(value);


                    case "code_msg":
                        mmsResponse.setCodeMsg(value);
                        break;

                    case "error_code":
                        mmsResponse.setErrorCode(value);
                        break;

                    case "remain":
                        mmsResponse.setRemain(value);
                        break;

                }
            }
        } catch (Exception exception) {
            throw new InternalServerErrorException(exception.getMessage(), exception);
        }

        mmsResponseMapper.insertMMSResponse(mmsResponse);

        ModelMapper modelMapper = new ModelMapper();
        MMSRequestDTO mmsDTO = modelMapper.map(mms, MMSRequestDTO.class);

        return mmsDTO;
    }

    private String iconvWithBase64Encoding(String fromCharset, String toCharset, String source) {
        String tmp = null;
        Charset euckrCharset = Charset.forName("euc-kr");
        ByteBuffer byteBuffer = euckrCharset.encode(source);
        byte[] euckrStringBuffer = new byte[byteBuffer.remaining()];
        byteBuffer.get(euckrStringBuffer);
        tmp = new String(Base64.getEncoder().encode(euckrStringBuffer));
        return tmp;
    }
}
