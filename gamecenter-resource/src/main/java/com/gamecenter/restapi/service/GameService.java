package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.Game;
import com.gamecenter.restapi.dto.GameDTO;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface GameService {
    List<GameDTO> getAllGames();
    GameDTO getGameById(Game game);
//    Game addGame(Game game,
//                 InputStream logoImgStream,
//                 FormDataContentDisposition logoImgMetaData,
//                 InputStream storyImgStream,
//                 FormDataContentDisposition storyImgMetaData,
//                 InputStream explationImgStream,
//                 FormDataContentDisposition explanationImgMetaData,
//                 InputStream gameZipStream,
//                 FormDataContentDisposition gameZipMetaData);

}
