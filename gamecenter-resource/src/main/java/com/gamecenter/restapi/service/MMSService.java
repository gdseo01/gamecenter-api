package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.MMSRequest;
import com.gamecenter.restapi.dto.MMSRequestDTO;

import java.io.IOException;

public interface MMSService {
    MMSRequestDTO sendMMSes(MMSRequest mms) throws IOException;
}
