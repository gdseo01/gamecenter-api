package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.Game;
import com.gamecenter.restapi.dto.GameDTO;
import com.gamecenter.restapi.mappers.GameMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.NotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class GameServiceImpl implements GameService {
    @Autowired
    private GameMapper gameMapper;

//    @Autowired
//    private S3Wrapper s3Wrapper;

    @Override
    @Transactional(readOnly = true)
    public List<GameDTO> getAllGames() {
        Game game = new Game();

        List<Game> games = gameMapper.selectGames(game);

        ModelMapper modelMapper = new ModelMapper();
        List<GameDTO> gameDTOs = games.stream().map(
                item -> {
                    return modelMapper.map(item, GameDTO.class);
                }
        ).collect(Collectors.toList());

// 204 리턴하기 위해 명시적으로 null 리턴.
        if (gameDTOs.isEmpty() == true) {
            return null;
        }

        return gameDTOs;
    }

    @Override
    @Transactional(readOnly = true)
    public GameDTO getGameById(Game game) {
        Game newGame = gameMapper.selectGameByGameId(game);

        if (newGame == null) {
            throw new NotFoundException("Game with gameId " + game.getGameId() + " Not Found.");
        }

        ModelMapper modelMapper = new ModelMapper();
        GameDTO newGameDTO = modelMapper.map(newGame, GameDTO.class);

        return newGameDTO;
    }

//    @Override
//    @Transactional(readOnly = false)
//    public synchronized Game addGame(
//            Game game
//            , InputStream logoImgStream
//            , FormDataContentDisposition logoImgMetaData
//            , InputStream storyImgStream
//            , FormDataContentDisposition storyImgMetaData
//            , InputStream explanationImgStream
//            , FormDataContentDisposition explanationImgMetaData
//            , InputStream gameZipStream
//            , FormDataContentDisposition gameZipMetaData) {
//
//        Date date = new Date();
//        String path = game.getCustomerId() + "/" + date.getTime();
//
//        if (logoImgMetaData != null) {
//            String destinationPath = path + "/logoImg" + "/" + logoImgMetaData.getFileName();
//            PutObjectResult putObjectResult = s3Wrapper.uploadByStream(logoImgStream, destinationPath);
//
//            if (putObjectResult.getETag() != null) {
//                game.setLogoImg(destinationPath);
//            }
//        } else {
//            game.setLogoImg("");
//        }
//
//        if (storyImgMetaData != null) {
//            String destinationPath = path + "/storyImg" + "/" + storyImgMetaData.getFileName();
//            PutObjectResult putObjectResult = s3Wrapper.uploadByStream(storyImgStream, destinationPath);
//
//            if (putObjectResult.getETag() != null) {
//                game.setStoryImg(destinationPath);
//            }
//        } else {
//            game.setStoryImg("");
//        }
//
//        if (explanationImgMetaData != null) {
//            String destinationPath = path + "/explanationImg" + "/" + explanationImgMetaData.getFileName();
//            PutObjectResult putObjectResult = s3Wrapper.uploadByStream(explanationImgStream, destinationPath);
//
//            if (putObjectResult.getETag() != null) {
//                game.setExplanationImg(destinationPath);
//            }
//        } else {
//            game.setExplanationImg("");
//        }
//
//        if (gameZipMetaData != null) {
//            String destinationPath = path + "/game";
//            String indexPath = s3Wrapper.uploadGame(gameZipStream, destinationPath);
//
//            if (indexPath != null) {
//                game.setUrl(destinationPath + "/" + indexPath);
//            }
//        } else {
//            game.setUrl("");
//        }
//
//
//        gameMapper.insertGame(game);
//        Game newGame = gameMapper.selectGameByGameId(game.getGameId());
//
//        if (newGame == null) {
//            throw new InternalServerErrorException("Failed add game.");
//        }
//
//        return newGame;
//    }

}
