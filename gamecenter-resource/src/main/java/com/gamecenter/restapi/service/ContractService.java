package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.Contract;
import com.gamecenter.restapi.dto.ContractDTO;

public interface ContractService {
    ContractDTO getContractById(Contract contract);

    Long addContract(Contract contract);
}
