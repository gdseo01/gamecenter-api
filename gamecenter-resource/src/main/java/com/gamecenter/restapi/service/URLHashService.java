package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.UrlHash;
import com.gamecenter.restapi.dto.UrlHashDTO;

public interface URLHashService {
    String addUrlHash(UrlHash urlHash);

    UrlHashDTO getUrlByHash(UrlHash urlHash);
}
