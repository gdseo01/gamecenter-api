package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.Contract;
import com.gamecenter.restapi.domain.GamePlay;
import com.gamecenter.restapi.domain.PlayRawData;
import com.gamecenter.restapi.dto.ContractDTO;
import com.gamecenter.restapi.dto.ContractGameOptionDTO;
import com.gamecenter.restapi.dto.RankDTO;
import com.gamecenter.restapi.mappers.ContractGameOptionMapper;
import com.gamecenter.restapi.mappers.ContractMapper;
import com.gamecenter.restapi.mappers.GamePlayMapper;
import com.gamecenter.restapi.mappers.PlayRawDataMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlaysServiceImpl implements PlaysService {

    @Autowired
    ContractMapper contractMapper;

    @Autowired
    ContractGameOptionMapper contractGameOptionMapper;

    @Autowired
    GamePlayMapper gamePlayMapper;

    @Autowired
    PlayRawDataMapper playRawDataMapper;

    @Override
    @Transactional(readOnly = true)
    public ContractDTO getCustomGameOptions(Contract contract) {
        Contract newContract = contractMapper.selectContractByContractId(contract);

        if (newContract == null) {
            throw new NotFoundException("계약 정보를 찾을수가 없습니다");
        }

        ModelMapper modelMapper = new ModelMapper();
        ContractDTO newContractDTO = modelMapper.map(newContract, ContractDTO.class);

        List<ContractGameOptionDTO> contractGameOptionDTOs = newContract.getContractGameOptions().stream().map(
                contractGameOption -> {
                    return modelMapper.map(contractGameOption, ContractGameOptionDTO.class);
                }).collect(Collectors.toList());

        newContractDTO.setContractGameOptions(contractGameOptionDTOs);

        return newContractDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean validateGameStart(Contract contract, GamePlay gamePlay) {
        Contract contract1 = contractMapper.selectContractByContractId(contract);

        if (contract1 == null) {
            throw new NotFoundException("Contract with contractId " + contract.getContractId() + " Not Found.");
        }

        if ("T".equals(contract1.getRestriction())) {
            gamePlay.setContractId(contract1.getContractId());
            gamePlay.setGameId(contract1.getGameId());

            int currentCount = gamePlayMapper.countGamePlayForGameStart(gamePlay);

            if (contract1.getRestrictionValue() <= currentCount && contract1.getRestrictionValue() > 0) {
                throw new ForbiddenException("더 이상 게임에 참여하실 수 없습니다");
            }
        }

        return true;
    }

    @Override
    @Transactional(readOnly = false)
    public void gameStart(GamePlay gamePlay) {
        try {
            insertPlayLog(gamePlay);

        } catch (Exception exception) {
            throw new InternalServerErrorException("게임을 시작할 수 없습니다.");
        }
    }

    @Override
    @Transactional(readOnly = false)
    public void gameEnd(GamePlay gamePlay) {
        try {
            insertPlayLog(gamePlay);

        } catch (Exception exception) {
            throw new InternalServerErrorException("게임 참여 정보가 저장되지 않았습니다.");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public RankDTO getCurrentRank(GamePlay gamePlay) {
        try {
            HashMap hashMap = gamePlayMapper.selectCurrentRank(gamePlay);

            ModelMapper modelMapper = new ModelMapper();
            RankDTO rankDTO = modelMapper.map(hashMap, RankDTO.class);

            return rankDTO;

        } catch (Exception exception) {
            throw new InternalServerErrorException("랭킹 정보를 조회할 수 없습니다.");
        }
    }

    private void insertPlayLog(GamePlay gamePlay) {
        gamePlayMapper.insertGamePlay(gamePlay);

        PlayRawData playRawData = new PlayRawData();
        playRawData.setActionType(gamePlay.getActionType());
        playRawData.setContractId(gamePlay.getContractId());
        playRawData.setGameId(gamePlay.getGameId());
        playRawData.setPlatformId(gamePlay.getPlatformId());
        playRawData.setUserKey(gamePlay.getUserKey());
        playRawData.setUserKeyType(gamePlay.getUserKeyType());
        playRawData.setNickname(gamePlay.getNickname());
        playRawData.setScore(gamePlay.getScore());

        playRawDataMapper.insertPlayRawData(playRawData);
    }
}
