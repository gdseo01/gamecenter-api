package com.gamecenter.restapi.service;

import com.gamecenter.restapi.domain.Contract;
import com.gamecenter.restapi.dto.ContractDTO;
import com.gamecenter.restapi.mappers.ContractGameOptionMapper;
import com.gamecenter.restapi.mappers.ContractMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractMapper contractMapper;

    @Autowired
    private ContractGameOptionMapper contractGameOptionMapper;

    @Override
    @Transactional(readOnly = true)
    public ContractDTO getContractById(Contract contract) {
        Contract newContract = contractMapper.selectContractByContractId(contract);

        if (newContract == null) {
            throw new NotFoundException("Contract with contractId " + contract.getContractId() + " Not Found.");
        }

        ModelMapper modelMapper = new ModelMapper();
        ContractDTO newContractDTO = modelMapper.map(newContract, ContractDTO.class);

        return newContractDTO;
    }

    @Override
    @Transactional(readOnly = false)
    public synchronized Long addContract(Contract contract) {
        try {
            Long[] optSeq = {1L};
            contract.setAdminId(0L);

            contractMapper.insertContract(contract);

            if (contract.getContractGameOptions().isEmpty() == false) {
                contract.getContractGameOptions().forEach(item -> {
                    item.setOptSeq(optSeq[0]);
                    item.setContractId(contract.getContractId());
                    item.setIsEnabled("T");
                    optSeq[0]++;
                });

                try {
                    contractGameOptionMapper.insertContractGameOptions(contract.getContractGameOptions());
                } catch (Exception e) {
                    throw new InternalServerErrorException("failed add contract game options.");
                }
            }

            return contract.getContractId();

        } catch (Exception e) {
            throw new InternalServerErrorException("failed add contract.");
        }

    }
}
