package com.gamecenter.restapi.mappers;

import com.gamecenter.restapi.domain.UrlHash;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface URLHashMapper {
    void insertUrlHash(UrlHash urlHash);

    UrlHash selectUrlHashByHash(UrlHash urlHash);
}
