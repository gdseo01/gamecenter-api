package com.gamecenter.restapi.mappers;

import com.gamecenter.restapi.domain.ContractGameOption;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ContractGameOptionMapper {
    void insertContractGameOptions(List<ContractGameOption> contractGameOptions);

}
