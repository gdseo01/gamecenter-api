package com.gamecenter.restapi.mappers;

import com.gamecenter.restapi.domain.Game;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GameMapper {
    List<Game> selectGames(Game game);

    Game selectGameByGameId(Game game);

    void insertGame(Game game);

}
