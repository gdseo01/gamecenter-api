package com.gamecenter.restapi.mappers;

import com.gamecenter.restapi.domain.MMSResponse;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MMSResponseMapper {

    void insertMMSResponse(MMSResponse mms);
}
