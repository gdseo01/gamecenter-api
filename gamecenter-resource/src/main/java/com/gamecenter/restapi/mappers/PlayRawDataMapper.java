package com.gamecenter.restapi.mappers;

import com.gamecenter.restapi.domain.PlayRawData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PlayRawDataMapper {

    void insertPlayRawData(PlayRawData playRawData);

}
