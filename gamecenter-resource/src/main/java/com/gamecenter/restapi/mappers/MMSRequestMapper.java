package com.gamecenter.restapi.mappers;

import com.gamecenter.restapi.domain.MMSRequest;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MMSRequestMapper {

    void insertMMSRequest(MMSRequest mms);
}
