package com.gamecenter.restapi.mappers;

import com.gamecenter.restapi.domain.GamePlay;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;

@Mapper
public interface GamePlayMapper {
    int countGamePlayForGameStart(GamePlay gamePlay);

    void insertGamePlay(GamePlay gamePlay);

    HashMap selectCurrentRank(GamePlay gamePlay);
}
