package com.gamecenter.restapi.mappers;

import com.gamecenter.restapi.domain.IAMUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IAMUserMapper {
    IAMUser selectIAMUser(String loginId);
}
