package com.gamecenter.restapi.mappers;

import com.gamecenter.restapi.domain.Contract;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ContractMapper {
    Contract selectContractByContractId(Contract contract);

    void insertContract(Contract contract);

}
