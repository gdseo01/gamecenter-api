package com.gamecenter.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author gdseo
 * 
 */
@SpringBootApplication(scanBasePackages = {"com.gamecenter.restapi"})
public class RestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestapiApplication.class, args);
	}
}

