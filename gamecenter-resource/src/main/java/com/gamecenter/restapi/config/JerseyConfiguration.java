package com.gamecenter.restapi.config;

import com.gamecenter.restapi.exception.GenericExceptionMapper;
import com.gamecenter.restapi.exception.JsonMappingExceptionMapper;
import com.gamecenter.restapi.exception.ValidationExceptionMapper;
import com.gamecenter.restapi.resource.*;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.validation.ValidationFeature;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 *
 */
@Configuration
public class JerseyConfiguration extends ResourceConfig {
	public JerseyConfiguration() {
	}

	@PostConstruct
	public void setUp() {
		register(ContractResource.class);
	    register(GameResource.class);
		register(HealthCheckResource.class);
		register(MMSResource.class);
		register(PlaysResource.class);
		register(URLHashResource.class);

		register(GenericExceptionMapper.class);
        register(JsonMappingExceptionMapper.class);
		register(ValidationExceptionMapper.class);

        register(JacksonFeature.class);
	    register(MultiPartFeature.class);
		register(ValidationFeature.class);

		register(CORSFilter.class);
	}

}
