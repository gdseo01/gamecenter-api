package com.gamecenter.restapi.domain;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class GameOption implements Serializable {

    private static final long serialiVersionUID = -1L;

    private Long gameId;

    private String optSeq;

    private String optName;

    private String optType;

    private String optValue;

    private String isDeleted;

    private Timestamp createdAt;

    private Timestamp updatedAt;

    private Timestamp deletedAt;
}
