package com.gamecenter.restapi.domain;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class UrlHash {

    private static final long serialVersionUID = -1L;

    private String hash;

    private String url;

    private Timestamp createdAt;
}
