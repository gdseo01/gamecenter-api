package com.gamecenter.restapi.domain;

import lombok.Data;
import org.springframework.data.domain.PageRequest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
public class Game implements Serializable {

    private static final long serialiVersionUID = -1L;

    private Long gameId;

    private Long customerId;

    private String title;

    private String url;

    private String logoImg;

    private String status;

    private String explanationImg;

    private String storyImg;

    private String description;

    private String isDeleted;

    private Timestamp createdAt;

    private Timestamp updatedAt;

    private Timestamp deletedAt;

    private List<GameOption> gameOptions;

}
