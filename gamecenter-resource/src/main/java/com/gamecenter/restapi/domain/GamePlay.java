package com.gamecenter.restapi.domain;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class GamePlay implements Serializable {

    private static final long serialVersionUID = -1L;

    private Long gamePlayId;

    private Long gameId;

    private Long contractId;

    private Long platformId;

    private String userKey;

    private String userKeyType;

    private String nickname;

    private String actionType;

    private int score;

    private Timestamp createdAt;
}
