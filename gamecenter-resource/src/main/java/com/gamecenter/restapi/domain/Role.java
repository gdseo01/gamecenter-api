package com.gamecenter.restapi.domain;

import lombok.Data;

@Data
public class Role {

    private Long roleId;

    private Long customerId;

    private String roleName;
}
