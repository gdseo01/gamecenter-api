package com.gamecenter.restapi.domain;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class ContractGameOption implements Serializable {

    private static final Long serialVersionUID = -1L;

    private Long contractId;

    private Long optSeq;

    private String optName;

    private String optType;

    private String optValue;

    private String isEnabled;

    private String isDeleted;

    private Timestamp createdAt;

    private Timestamp updatedAt;

    private Timestamp deletedAt;

}
