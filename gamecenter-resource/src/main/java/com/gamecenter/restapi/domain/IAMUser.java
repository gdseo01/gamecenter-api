package com.gamecenter.restapi.domain;

import lombok.Data;

import java.util.List;

@Data
public class IAMUser {

    private Long customerId;

    private String loginId;

    private String password;

    private List<Role> roles;

}
