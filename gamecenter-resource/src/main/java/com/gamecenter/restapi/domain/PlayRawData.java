package com.gamecenter.restapi.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Data
public class PlayRawData implements Serializable {

    private static final long serialVersionUID = -1L;

    private Long idx;

    private String userKey;

    private String userKeyType;

    private String nickname;

    private Long contractId;

    private Long gameId;

    private int times;

    private Long customerId;

    private String customerName;

    private String chargingType;

    private BigDecimal chargingPrice;

    private Date startDate;

    private Date endDate;

    private String restriction;

    private int restrictionValue;

    private Long platformId;

    private String actionType;

    private int score;

    private Timestamp createAt;

}
