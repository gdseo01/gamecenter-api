package com.gamecenter.restapi.domain;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
public class Contract implements Serializable {

    private static final long serialVersionUID = -1L;

    private Long contractId;

    private Long gameId;

    private Long customerId;

    private int times;

    private String chargingType;

    private int chargingPrice;

    private Timestamp startDate;

    private Timestamp endDate;

    private String endpointUrl;

    private String restriction;

    private int restrictionValue;

    private String winnerMethod;

    private int winnerCount;

    private String winnerSpecificRank;

    private Long adminId;

    private String isDeleted;

    private Timestamp createdAt;

    private Timestamp updatedAt;

    private Timestamp deletedAt;

    private String gameUrl;

    private List<ContractGameOption> contractGameOptions;
}
