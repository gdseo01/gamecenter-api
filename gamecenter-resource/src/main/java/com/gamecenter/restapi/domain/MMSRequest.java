package com.gamecenter.restapi.domain;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class MMSRequest {

    private Long mmsRequestId;

    private String gubun;

    private String sendPhone;

    private String recvPhone;

    private String subject;

    private String msg;

    private Timestamp createdAt;
}
