package com.gamecenter.restapi.domain;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class MMSResponse {

    private Long mmsResponseId;

    private Long mmsRequestId;

    private String msgKey;

    private String code;

    private String codeMsg;

    private String errorCode;

    private String remain;

    private String sendKey;

    private Timestamp createdAt;
}
