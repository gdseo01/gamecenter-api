package com.gamecenter.restapi.profilesysout;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("development")
public class DevelopmentProfileSysOut {

    @Autowired
    public DevelopmentProfileSysOut(@Value("${spring.profile.message}") String msg) {
        System.out.println("##################################");
        System.out.println("##################################");
        System.out.println("##              DEV             ##");
        System.out.println(msg);
        System.out.println("##################################");
        System.out.println("##################################");
    }
}
