package com.gamecenter.restapi;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.gamecenter.restapi.enumerate.FileMimeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.apache.commons.io.FilenameUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Service
public class S3Wrapper {
    @Autowired
    private AmazonS3Client amazonS3Client;

    @Value("${cloud.aws.s3.bucket}")
    private String bucket;

    private PutObjectResult upload(String filePath, String uploadKey, ObjectMetadata metadata) throws FileNotFoundException {
        return upload(new FileInputStream(filePath), uploadKey, metadata);
    }


    private PutObjectResult upload(InputStream inputStream, String uploadKey, ObjectMetadata metadata) {
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, uploadKey, inputStream, metadata);
        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
        PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);
        IOUtils.closeQuietly(inputStream, null);

        return putObjectResult;
    }


    public PutObjectResult uploadByStream(InputStream multipartFile, String path) {
        PutObjectResult putObjectResult = null;
        String mimeType = FileMimeType.fromExtension(FilenameUtils.getExtension(path)).mimeType();

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(mimeType);

        try {
            putObjectResult = upload(multipartFile, path, metadata);
            multipartFile.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return putObjectResult;
    }


    public String uploadGame(InputStream multipartFile, String uploadKey) {
        byte[] buffer = new byte[1024];
        String indexPath = null;

        try {
            ZipInputStream zipInputStream = new ZipInputStream(multipartFile);
            ZipEntry zipEntry = zipInputStream.getNextEntry();

            while (zipEntry != null) {
                String fileName = zipEntry.getName();

                if (fileName.matches("(.*)index.html$")) {
                    indexPath = fileName;
                }

                String mimeType = FileMimeType.fromExtension(FilenameUtils.getExtension(fileName)).mimeType();

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                int len;

                while ((len = zipInputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, len);
                }

                InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
                ObjectMetadata metadata = new ObjectMetadata();

                metadata.setContentLength(outputStream.size());
                metadata.setContentType(mimeType);

                upload(inputStream, uploadKey + "/" + fileName, metadata);

                inputStream.close();
                outputStream.close();

                zipEntry = zipInputStream.getNextEntry();
            }

            zipInputStream.closeEntry();
            zipInputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return indexPath;
    }


    public Response download(String key) throws IOException {
        GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);
        S3Object s3Object = amazonS3Client.getObject(getObjectRequest);
        S3ObjectInputStream objectInputStream = s3Object.getObjectContent();
        byte[] bytes = IOUtils.toByteArray(objectInputStream);
        String fileName = URLEncoder.encode(key, "UTF-8").replaceAll("\\+", "%20");

        return Response
                .ok(objectInputStream, MediaType.APPLICATION_OCTET_STREAM)
                .header("content-disposition","attachment; filename = " + fileName)
                .build();
    }


    public List<S3ObjectSummary> list() {
        ObjectListing objectListing = amazonS3Client.listObjects(new ListObjectsRequest().withBucketName(bucket));
        List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();
        return s3ObjectSummaries;
    }
}
