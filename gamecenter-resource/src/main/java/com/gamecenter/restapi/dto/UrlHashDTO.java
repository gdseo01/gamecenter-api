package com.gamecenter.restapi.dto;

import lombok.Data;

@Data
public class UrlHashDTO {

    private String hash;

    private String url;

}
