package com.gamecenter.restapi.dto;

import lombok.Data;

import java.util.Base64;

@Data
public class GamePlayDTO {

    private Long gamePlayId;

    private Long gameId;

    private Long contractId;

    private Long platformId;

    private String userKey;

    private String userKeyType;

    private String nickname;

    private String actionType;

    private int score;

    public String getUserKey() {
        return this.userKey;
    }

    public void setUserKey(String userKey) {
        byte[] decodedBytes = Base64.getDecoder().decode(userKey);

        this.userKey = new String(decodedBytes);

    }

    public void setNickname(String nickname) {
        byte[] decodedBytes = Base64.getDecoder().decode(nickname);

        this.nickname = new String(decodedBytes);

    }

    public String getNickname() {
        return this.nickname;
    }

}
