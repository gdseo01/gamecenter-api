package com.gamecenter.restapi.dto;

import lombok.Data;

@Data
public class ContractGameOptionDTO {
    private Long contractId;

    private String optName;

    private String optType;

    private String optValue;

}
