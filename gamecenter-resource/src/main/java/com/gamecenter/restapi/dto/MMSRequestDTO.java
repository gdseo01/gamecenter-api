package com.gamecenter.restapi.dto;

import lombok.Data;

@Data
public class MMSRequestDTO {

    private Long mmsRequestId;

    private String gubun;

    private String sendPhone;

    private String recvPhone;

    private String subject;

    private String msg;

}
