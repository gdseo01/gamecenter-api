package com.gamecenter.restapi.dto;

import lombok.Data;

@Data
public class GameDTO {
    private Long gameId;

    private String title;

    private String url;

    private String logoImg;

    private String status;

    private String explanationImg;

    private String storyImg;

}
