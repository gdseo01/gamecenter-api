package com.gamecenter.restapi.dto;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class ContractDTO {
    private Long contractId;

    private Long gameId;

    private int times;

    private String chargingType;

    private int chargingPrice;

    private Timestamp startDate;

    private Timestamp endDate;

    private String endpointUrl;

    private String gameUrl;

    private String restriction;

    private int restrictionValue;

    private String winnerMethod;

    private int winnerCount;

    private String winnerSpecificRank;

    private List<ContractGameOptionDTO> contractGameOptions;

}
