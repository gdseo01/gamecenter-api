package com.gamecenter.restapi.dto;

import lombok.Data;

@Data
public class RankDTO {

    private String userKey;

    private Long score;

    private Integer rank;

}
