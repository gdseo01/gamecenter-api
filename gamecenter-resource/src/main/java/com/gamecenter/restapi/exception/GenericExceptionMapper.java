package com.gamecenter.restapi.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * 
 */
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

	@Override
	public Response toResponse(Throwable exception) {
	    exception.printStackTrace();
        Response.StatusType type = getStatusType(exception);

        ErrorMessage errorMessage = new ErrorMessage(exception.getLocalizedMessage(), type.getStatusCode(), "");

        return Response.status(errorMessage.getErrorCode())
                .entity(errorMessage)
                .type("application/json")
                .build();
	}

    private Response.StatusType getStatusType(Throwable exception) {
        if (exception instanceof WebApplicationException) {
            return((WebApplicationException)exception).getResponse().getStatusInfo();
        } else {
            return Response.Status.INTERNAL_SERVER_ERROR;
        }
    }

}
