package com.gamecenter.restapi.exception;

import org.glassfish.jersey.server.validation.ValidationError;
import org.glassfish.jersey.server.validation.internal.ValidationHelper;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.List;

public final class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

    @Override
    public Response toResponse(final ValidationException exception) {
        if (exception instanceof ConstraintViolationException) {

            final ConstraintViolationException cve = (ConstraintViolationException) exception;
            List<ValidationError> validationErrors = ValidationHelper.constraintViolationToValidationErrors(cve);

            ErrorMessage errorMessage = new ErrorMessage(validationErrors.iterator().next().getMessage(), ValidationHelper.getResponseStatus(cve).getStatusCode(), "");

            return Response.status(ValidationHelper.getResponseStatus(cve))
                   .entity(errorMessage)
                   .type(MediaType.APPLICATION_JSON)
                   .build();

        } else {
            return Response.serverError().entity(exception.getMessage()).build();

        }
    }
}
