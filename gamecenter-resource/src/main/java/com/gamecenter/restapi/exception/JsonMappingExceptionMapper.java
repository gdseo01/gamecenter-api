package com.gamecenter.restapi.exception;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class JsonMappingExceptionMapper implements ExceptionMapper<JsonMappingException> {
    @Override
    public Response toResponse(JsonMappingException exception) {
        InvalidFormatException invalidFormatException = (InvalidFormatException) exception;

        String msg = String.format("Can not deserialize value of %s from \"%s\".",
                invalidFormatException.getTargetType().getName(),
                invalidFormatException.getValue().toString());

        ErrorMessage errorMessage = new ErrorMessage(msg, Response.Status.BAD_REQUEST.getStatusCode(), "");

        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).type(MediaType.APPLICATION_JSON).build();
    }
}
