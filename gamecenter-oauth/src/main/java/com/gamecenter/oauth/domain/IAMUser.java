package com.gamecenter.oauth.domain;

import lombok.Data;

@Data
public class IAMUser {

    private Long customerId;

    private String loginId;

    private String password;
}
