package com.gamecenter.oauth.domain;

import lombok.Data;

@Data
public class Role {

    private Long roleId;

    private Long customerId;

    private String roleName;
}
