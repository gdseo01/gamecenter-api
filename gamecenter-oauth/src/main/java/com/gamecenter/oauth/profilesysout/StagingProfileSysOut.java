package com.gamecenter.oauth.profilesysout;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("staging")
public class StagingProfileSysOut {

    @Autowired
    public StagingProfileSysOut(@Value("${spring.profile.message}") String msg) {
        System.out.println("##################################");
        System.out.println("##################################");
        System.out.println("##            Staging           ##");
        System.out.println(msg);
        System.out.println("##################################");
        System.out.println("##################################");
    }
}
