package com.gamecenter.oauth.mappers;

import com.gamecenter.oauth.domain.IAMUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IAMUserMapper {
    IAMUser selectIAMUser(String loginId);
}
