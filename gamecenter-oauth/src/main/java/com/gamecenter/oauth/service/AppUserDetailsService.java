package com.gamecenter.oauth.service;

import com.gamecenter.oauth.domain.Role;
import com.gamecenter.oauth.mappers.IAMUserMapper;
import com.gamecenter.oauth.mappers.RoleMapper;
import com.gamecenter.oauth.domain.IAMUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppUserDetailsService implements UserDetailsService {
    @Autowired
    private IAMUserMapper iamUserMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public UserDetails loadUserByUsername(String loginId) throws UsernameNotFoundException {
        IAMUser iamUser = iamUserMapper.selectIAMUser(loginId);

        if(iamUser == null) {
            throw new UsernameNotFoundException(String.format("The login id %s doesn't exist", loginId));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();

        Role role = new Role();
        role.setCustomerId(iamUser.getCustomerId());

        roleMapper.selectRoleByCustomerId(role.getCustomerId()).forEach(roles -> {
            authorities.add(new SimpleGrantedAuthority(roles.getRoleName()));
        });

        UserDetails userDetails = new AppUser(iamUser.getLoginId(), iamUser.getPassword(), authorities, iamUser.getCustomerId());

        return userDetails;
    }
}
