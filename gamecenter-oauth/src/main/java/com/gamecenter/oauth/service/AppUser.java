package com.gamecenter.oauth.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class AppUser extends User {

    private Long customerId;

    public AppUser(String username
            , String password
            , Collection<? extends GrantedAuthority> authorities
            , Long customerId) {
        super(username, password, authorities);

        this.customerId = customerId;
    }

    public Long getCustomerId() {
        return this.customerId;
    }
}
